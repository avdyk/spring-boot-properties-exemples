# Spring Boot properties exemples


## Introduction

Ce projet montre les différentes manières de configurer un projet Spring Boot.

Nous en profitons aussi pour montrer les bonnes pratiques de _logging_, ainsi que le monitoring d'une application 
spring boot avec _l'actuator_.


## Le webservice random

Le webservice _random_ génère une série de nombres aléatoires entre un
palier minimum et maximum. Les trois valeurs sont configurables
(_min_, _max_, _size_).

Référence: https://mkyong.com/java/java-generate-random-integers-in-a-range/


## Configuration du projet

En plus des variables spring-boot, on peut définir les variables suivantes
(les assignations sont les valeurs par défaut:

    sbpe.random.min=0
    sbpe.random.max=100
    sbpe.random.size=10

## Configuration du logging

On peut passer des variables d'environnement pour configurer le logging
(`logback`).

Par exemple, par défaut, le logging sera au level *INFO*.
Mais pour activer le niveau *TRACE* sur notre classe de web service,
on peut passer cet argument:

    --logging.level.be.liege.cti.tuto.spring.boot.properties.exemples.RandomWebService=trace


## Comment lancer le projet

### Avec maven

    $ mvn spring-boot:run -Dspring-boot.run.arguments="--sbpe.random.min=0"

Il est aussi possible de mettre en place les variables d'environnement de la console pour exécuter l'application. Les
deux passages de paramètres ne sont pas exclusifs:

    $ SBPE_RANDOM_MAX=10 mvn spring-boot:run -Dspring-boot.run.arguments="--sbpe.random.min=0"

### Avec IntelliJ

La classe `SprintBootPropertiesExemplesApplication` de notre projet est marquée comme `@SpringBootApplication`. 
IntelliJ reconnait là un point d'entrée pour une application _Spring Boot_. Il place donc un petit triangle vert sur 
le logo _C_ de la classe.

Clique droit sur la classe et on peut lancer l'application.

On peut éditer le lanceur `Run > Edit Configurations...`
![Configuration du lanceur](doc/configuration-launcher-springboot.png "Configuration du lanceur d'applications")

Dans notre cas, ce qui nous intéresse:

* passer des arguments au programme
* mettre en place des variables d'environnement
* cocher la case du _Include dependencies with "Provided" scope_ (dans le cas d'une application web,
  sinon, une fois que le serveur est lancé, il s'arrête!)

Resultat:

![Une fois l'application lancée](doc/running-springboot-app.png "Une fois l'application lancée")

### Depuis la console

Même s'il s'agit d'un `war`, il est possible de lancer l'application de manière autonome.

Après avoir compilé l'application: `mvn package`, il est possible de la lancer avec la commande `java`:

    $ java -jar spring-boot-properties-exemples.war --sbpe.random.min=205

Avec ce paramètre, comme le paramètre du _max_ est de 100, que nous définissons _min_ plus grand que _max_,
l'application ne démarrera pas.

#### Un fichier de configuration alternatif

Si un fichier `application.properties` est lisible dans le même répertoire que le `(j|w)ar`, les valeurs
définies dans ce fichier vont redéfinir les valeurs par défaut.

Il est possible de spécifier un fichier de configuration en modifiant la propriété `spring.config.location`

Référence: https://www.baeldung.com/properties-with-spring

Pour le changement de _banner_, voir https://www.baeldung.com/spring-boot-disable-banner et aussi
[Génération d'ascii art](http://patorjk.com/software/taag/#p=display&f=Star%20Wars&t=Ville%20de%20Liege)

Il est aussi possible d'écrire les fichiers de configuration en `yaml`, qui permet une syntaxe en hiérarchie.

Il est aussi possible, dans le code, de définir un ou des chemins alternatifs pour les fichiers
de configuration ([Réponse sur StackOverflow](https://stackoverflow.com/a/55231297/1380534))

### Application autonome jar selfexecutable

Un profile permettant de rendre le jar exécutable est préparé dans le `pom.xml`.
Pour l'activé, il faut construire le projet telque:

    $ mvn package -P standalone

Dans le répertoire `target`, il y a une archive `spring-boot-properties-exemples-0.0.1-SNAPSHOT.war`.
En lisant le fichier avec la commande `less`, par exemple, on remarque que le début
du fichier est en réalité un script `bash`. Une fois les droits en exécution donnés
au script, il est possible de l'exécuter

    $ cd target ; ./spring-boot-properties-exemple-0.0.1-SNAPSHOT.war

On peut aussi mettre en place des variables d'environnement ou bien lui passer des arguments.

### Déployer un service avec systemd

Copier le fichier `spring-boot-properties-exemples.service` dans le répertoire
`/etc/systemd/system`, relancer `systemd`:

    # systemctl daemon-reload

Ensuite, on peut armer le service et le démarrer

    # systemctl enable mon-application.service
    # systemctl start mon-application.service

L'exemple de fichier de configuration a été trouvé ici:
https://docs.spring.io/spring-boot/docs/current/reference/html/deployment-install.html

### Déployer le war dans un Tomcat

L'application spring boot peut aussi se déployer dans une instance de _Tomcat_ (ou _Jetty_).
Il suffit de déposer dans le répertoire `webapps` le fichier `war` généré.

On peut aussi configurer l'application web dans _Tomcat_ en ajoutant le fichier attendu
dans une des classes de configuration de spring boot. Dans notre cas, voir le code

    @PropertySources({
            @PropertySource(value = "file:${catalina.base}/conf/vdl/sbpe/exemple.conf",
                    ignoreResourceNotFound = true)
    })

Il suffira alors de déposer le fichier de configuration dans le répertoire
`$CATALINA_BASE/conf/vdl/sbpe/exemple.conf` (dans notre cas).

Notez bien qu'il faut utiliser `$CATALINA_BASE`, qui est l'emplacement de l'instance de tomcat
et non `$CATALINA_HOME` qui est l'emplacement des fichiers principaux. Pour rappel, il est possible
de lancer plusieurs instances de _Tomcat_ à partir du même __code base__.

### Création d'une image docker avec l'application Spring-Boot

- https://spring.io/guides/gs/spring-boot-docker/

- https://spring.io/blog/2020/01/27/creating-docker-images-with-spring-boot-2-3-0-m1

- https://mkyong.com/docker/docker-spring-boot-examples/

- https://dzone.com/articles/build-package-and-run-spring-boot-apps-with-docker

1. Configurer le projet pour construire une image

2. Comment lancer Docker pour accéder à l'application web

## Références

- Spring
  - [Spring Boot Features](https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html)
    - [Spring Boot Configuration externe](https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-external-config)
    - [Spring Boot Application Property Files](https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-external-config-application-property-files)
  - [Common Application properties](https://docs.spring.io/spring-boot/docs/current/reference/html/appendix-application-properties.html)
  - [“How-to” Guides](https://docs.spring.io/spring-boot/docs/current/reference/html/howto.html)
  - [Déploiement](https://docs.spring.io/spring-boot/docs/current/reference/html/deployment-install.html)
  - Baeldung
    - [Baeldung sur les `properties`](https://www.baeldung.com/?s=properties)
  - Mkyong
    - [Générer des entiers aléatoires](https://mkyong.com/java/java-generate-random-integers-in-a-range/)

- [AssertJ](https://assertj.github.io/doc/)
