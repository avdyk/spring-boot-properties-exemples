package be.liege.cti.tuto.spring.boot.properties.exemples;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

/**
 * Classe de lancement de l'application spring-boot.
 *
 * @see <a href="https://www.baeldung.com/spring-boot-command-line-arguments">https://www.baeldung.com/spring-boot-command-line-arguments</a>
 */
@SpringBootApplication
@PropertySources({
        @PropertySource(value = "file:${catalina.base}/conf/vdl/sbpe/exemple.conf",
                ignoreResourceNotFound = true)
})
public class SprintBootPropertiesExemplesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SprintBootPropertiesExemplesApplication.class, args);
    }

}
