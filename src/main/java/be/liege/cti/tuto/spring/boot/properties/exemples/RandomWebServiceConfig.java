package be.liege.cti.tuto.spring.boot.properties.exemples;

/**
 * Configuration du webservice.
 */
interface RandomWebServiceConfig {

  String RANDOM_MIN = "sbpe.random.min";
  String RANDOM_MAX = "sbpe.random.max";
  String RANDOM_SIZE = "sbpe.random.size";
  String MIN_SPRING = "${" + RANDOM_MIN + ":0}";
  String MAX_SPRING = "${" + RANDOM_MAX + ":100}";
  String SIZE_SPRING = "${" + RANDOM_SIZE + ":10}";

}
