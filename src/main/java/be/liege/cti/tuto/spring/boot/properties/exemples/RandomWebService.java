package be.liege.cti.tuto.spring.boot.properties.exemples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.Collectors;

import static be.liege.cti.tuto.spring.boot.properties.exemples.RandomWebServiceConfig.MAX_SPRING;
import static be.liege.cti.tuto.spring.boot.properties.exemples.RandomWebServiceConfig.MIN_SPRING;
import static be.liege.cti.tuto.spring.boot.properties.exemples.RandomWebServiceConfig.SIZE_SPRING;

/**
 * Ce webservice génère une série de nombres aléatoires.
 *
 * @see #random()
 * @see <a href="https://spring.io/guides/gs/rest-service/">https://spring.io/guides/gs/rest-service/</a>
 */
@RestController()
public class RandomWebService {

    private static final Logger logger = LoggerFactory.getLogger(RandomWebService.class);

    private final int min;
    private final int max;
    private final int size;

    /**
     * Construction du webservice avec les paramètres de configuration.
     * La construction du webservice avec les paramètres permet de s'assurer
     * que l'on vérifiera les paramètres passés. L'application ne démarrera
     * pas si elle n'est pas bien configurée.
     *
     * Pour les paramètres obligatoires à une webservice, il est conseillé d'utiliser les propriétés
     * <code>private final</code> et de les mettre en place via le constructeur.
     *
     * @param min  la borne minimale du nombre aléatoire.
     * @param max  la borne maximale du nombre aléatoire (doit être plus grand ou égal à <code>min</code>).
     * @param size la taille de la série de nombres aléatoires à créer
     * @see <a href="https://www.baeldung.com/spring-value-defaults">https://www.baeldung.com/spring-value-defaults</a>
     */
    public RandomWebService(@Value(MIN_SPRING) final int min,
                            @Value(MAX_SPRING) final int max,
                            @Value(SIZE_SPRING) final int size) {
        logger.info("Configuration du service random: min={};max={};size={}", min, max, size);
        if (max <= min) {
            final String msg = String.format("Max (%d) est plus petit ou égal à min (%d)!", min, max);
            logger.error(msg);
            throw new IllegalArgumentException(msg);
        }
        if (size <= 0) {
            final String msg = String.format("Size (%d) est inférieur ou égal à zéro!", size);
            logger.error(msg);
            throw new IllegalArgumentException(msg);
        }
        this.min = min;
        this.max = max;
        this.size = size;
    }

    /**
     * Méthode qui retourne une série de nombre aléatoires.
     *
     * @return un tableau de taille <code>size</code>, dont chaque nombre est un nombre aléatoire entre
     * <code>min</code> &gt;= <code>max</code>>.
     */
    @GetMapping("/random")
    public int[] random() {
        assert min < max;
        assert size > 0;
        logger.trace("Appel à la méthode random()");
        int[] ints = new Random().ints(size, min, max)
                .toArray();
        logger.debug("{} nombre(s) aléatoires ont été générés", ints.length);
        if (logger.isTraceEnabled()) {
            final String out = Arrays.stream(ints)
                    .mapToObj(String::valueOf)
                    .collect(Collectors.joining(";"));
            logger.trace("Nombres générés: {}", out);
        }
        return ints;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    public int getSize() {
        return size;
    }
}
