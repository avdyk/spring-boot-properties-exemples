package be.liege.cti.tuto.spring.boot.properties.exemples;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;

import java.util.Arrays;

/**
 * Lancement des tests spring-boot.
 *
 * @see <a href="https://www.baeldung.com/spring-boot-command-line-arguments">https://www.baeldung.com/spring-boot-command-line-arguments</a>
 * @see <a href="https://assertj.github.io/doc/">https://assertj.github.io/doc/</a>
 */
@SpringBootTest(args = {"--spring.main.banner-mode=off",
        "--sbpe.random.min=" + SprintBootPropertiesExemplesApplicationTests.MINIMUM_EXPECTED})
class SprintBootPropertiesExemplesApplicationTests {

    public static final int MINIMUM_EXPECTED = 42;

    @Test
    void contextLoads() {
    }

    @Test
    public void whenUsingSpringBootTestArgs_thenCommandLineArgSet(@Autowired Environment env) {
        Assertions.assertThat(env.getProperty("spring.main.banner-mode"))
                .as("Vérifie que le mode 'banner' spring-boot n'est pas activé")
                .isEqualTo("off");
    }

    @Test
    public void whenUsingSpringBootTestArgs_thenRandomMinIs42(@Autowired RandomWebService service) {
        Assertions.assertThat(service.getMin())
                .as("Vérifie que le minimum correspond bien à %d, comme configuré", MINIMUM_EXPECTED)
                .isEqualTo(MINIMUM_EXPECTED);
    }

    @Test
    public void byDefault_RandomMaxIs100(@Autowired RandomWebService service) {
        Assertions.assertThat(service.getMax())
                .as("Vérifie que le maximum correspond à la valeur par défaut (100)")
                .isEqualTo(100);
    }

    @Test
    public void byDefault_RandomSizeIs10(@Autowired RandomWebService service) {
        Assertions.assertThat(service.getSize())
                .as("Vérifie que la taille par défaut est bien 10")
                .isEqualTo(10);
    }

    @Test
    public void randomListSizeIs10Elements(@Autowired RandomWebService service) {
        Assertions.assertThat(service.random())
                .as("Vérifie qu'il y a bien 10 nombres aléatoires générés (configuration par défaut")
                .hasSize(10);
    }

    @Test
    public void randomListMinimumValueIs42(@Autowired RandomWebService service) {
        final int minimumValue = Arrays.stream(service.random())
                .min()
                .orElseThrow(RuntimeException::new);
        Assertions.assertThat(minimumValue)
                .as("Vérifie qu'il n'y a pas de nombre inférieur à %d dans la suite", MINIMUM_EXPECTED)
                .isGreaterThanOrEqualTo(MINIMUM_EXPECTED);
    }

    @Test
    public void randomListMaximumValueIs100(@Autowired RandomWebService service) {
        final int maximumValue = Arrays.stream(service.random())
                .max()
                .orElseThrow(RuntimeException::new);
        Assertions.assertThat(maximumValue)
                .as("Vérifie qu'il n'y a pas de nombre supérieur à 100 dans la suite")
                .isLessThanOrEqualTo(100);
    }

}
